package cn.edu.ujn.ch9.dao;

import java.util.List;

public class Orders {
    private Integer id;

    private String number;

    private Integer userId;
    
    private User user;
    
    private List<Product> productList;
    
    

    public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

	@Override
	public String toString() {
		return "Orders [id=" + id + ", number=" + number + ", userId=" + userId + ", user=" + user + ", productList="
				+ productList + "]";
	}



    
}