package cn.edu.ujn.ch9.dao;

import java.util.List;

public class User {
    private Integer id;

    private String name;

    private String password;
    
    private List<Orders>  orderList;
    
    

    @Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", password=" + password + ", orderList=" + orderList + "]";
	}

	public List<Orders> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<Orders> orderList) {
		this.orderList = orderList;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }
}